# Sample NODE JS application

Here are required steps to build, run and deploy this Sample NodeJS app.

###### Install the libraries and dependencies
```
npm install
```

Set SAUCE LABS credentials and HOST URL
```
export SAUCE_USER=your_saucelabs_username_here
export SAUCE_KEY=your_saucelabs_access_key_here
```

Set Gitlab Runner token
```
export GITLAB_RUNNER_TOKEN=YOUR_TOKEN_GOES_HERE
```

###### Build the application
```
npm run build
```

###### Test the application
```
npm run test
```

###### Test the application with Sauce Labs
```
npm run test-ui
```

###### Start the application
```
npm run start
```

## Docker image

In order to generate a docker image for the sample app, build it first:
```
#make docker_pre_build
make docker_build
```

If you need to run the image:
```
make docker_run
```

In order to push the image to Google Cloud repository
```
make docker_release
``` 

## Helm application

Deploy the sample application to use with the jenkins integration.

```
cd helm
make deploy
```

## Jenkins integration

This repository contains a `Jenkinsfile` which contains all instructions for jenkins in order to:
- Build
- Test
- Build docker image
- Push docker image
- Deploy image to kubernetes


## Gitlab Runner integration

In order to run a Gitlab runner locally make sure you exported `GITLAB_RUNNER_TOKEN` environment variable

#### Run the Gitlab Runner

This command is required to run the gitlab runner and register it: 

```
make gitlab_runner
```

Make sure you got this message after running the command:
>Registering runner... succeeded                     runner=4PDam89o

>Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!


### Unregister gitlab Runner

In order to unregister the runner then run the following command:
```
make gitlab_runner_unregister
```

Make sure the output of the command is similar to:
> WARNING: Unregistering all runners

> Unregistering runner from GitLab succeeded          runner=fbea57ab


