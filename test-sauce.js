require('dotenv').config()

var webdriver = require('selenium-webdriver'),
  SauceLabs = require("saucelabs"),
  saucelabs = new SauceLabs({
    username: process.env.SAUCE_USERNAME,
    password: process.env.SAUCE_ACCESS_KEY
  });

var appUri = process.env.APP_URI;
//var appUri = "https://sampleapp-dev.volterra.io";

var sauce = 'http://ondemand.saucelabs.com:80/wd/hub';
var driver = new webdriver.Builder().
    usingServer(sauce).
    withCapabilities({
        browserName: 'Chrome',
        platform: 'Windows 2012',
        name: 'Sample selenium-webdriver test',
        username: process.env.SAUCE_USERNAME,
        accessKey: process.env.SAUCE_ACCESS_KEY
    }).
    build();

console.log("Testing URL: " + appUri);
console.log("Sauce USER: " + process.env.SAUCE_USER);
driver.get(appUri);

var expectedTitle = 'I am a page title - Sauce Labs';
driver.sleep(2000).then(function() {
	
  driver.getTitle().then(function(title) {
    var testResult = (title === expectedTitle);
		
    saucelabs.updateJob(driver.sessionID, {
      name: 'Sample nodejs app title test',
      passed: testResult
    });

    if(testResult) {
      console.log('Test passed');
    } else {
      console.log('Test failed');
			console.log("Expected: \"" + expectedTitle + "\"\nActual: \"" + title + "\"");
    }

    process.exit(testResult ? 0 : 1);
  });
});

driver.quit();
