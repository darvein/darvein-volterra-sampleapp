GCR = us.gcr.io
VERSION = 1.0.0
LOCAL_PORT = 3000
EXPOSE_PORT = 3000

SERVICE_NAME = "sample-nodejs"
GC_PROJECT = `gcloud config get-value project`
IMAGE_NAME = $(GCR)/$(GC_PROJECT)/$(SERVICE_NAME)

GITLABRUNNERTOKEN = ${GITLAB_RUNNER_TOKEN}

.PHONY: all \
				deps build start test test-ui test-all deploy pipeline pipeline-full \
				docker_pre_build docker_build docker_tag_latest docker_release docker_run docker_deploy

all: build

deps: package.json
	@echo "[+] Install required dependencies"
	npm install

build: deps 
	@echo "[+] Building the project"
	npm run build

start: build
	@echo "[+] Starting the app"
	npm run start

test:
	@echo "[+] Running unit tests"
	npm run test

test-ui: .env
	@echo "[+] Running UI tests"
	npm run test-ui

test-all: test test-ui

deploy:
	@echo "[+] Deploying"

pipeline: deps build test

pipeline-full: pipeline test-ui

docker_pre_build:
	@echo "[+] Pre-Build: Copy the APPLICATION source files"
	cp -R ../app/ ./src

docker_build:
	@echo "[+] Building $(IMAGE_NAME):$(VERSION)"
	docker build -t "$(IMAGE_NAME):$(VERSION)" --rm .

docker_tag_latest:
	@echo "[+] Tagging $(IMAGE_NAME) as latest"
	docker tag "$(IMAGE_NAME):$(VERSION)" "$(IMAGE_NAME):latest"

docker_run: stop
	@echo "[+] Running $(SERVICE_NAME)"
	docker run --name $(SERVICE_NAME) -p $(LOCAL_PORT):$(EXPOSE_PORT) -it -d "$(IMAGE_NAME):$(VERSION)"

docker_stop:
	@echo "[+] Stopping and destroing $(SERVICE_NAME)"
	@if docker ps | grep $(SERVICE_NAME); then echo "[+] Shuting down $(SERVICE_NAME)"; docker stop $(SERVICE_NAME); docker rm $(SERVICE_NAME); fi

docker_release: docker_tag_latest
	@if ! docker images $(IMAGE_NAME) | awk '{ print $$2 }' | grep -q -F $(VERSION); then echo "$(IMAGE_NAME) version $(VERSION) is not yet built. Please run 'make build'"; false; fi
	@echo "[+]  Releasing $(IMAGE_NAME) to Registry"
	gcloud docker -- push $(IMAGE_NAME)

deploy:
	@echo "[+] Deploying $(IMAGE_NAME):latest"

gitlab_runner: gitlab_runner_unregister
	@echo "[+] Running Gitlab Runner into a Docker container"
	cd gitlab-runner && docker build --build-arg GITLAB_RUNNER_TOKEN=${GITLABRUNNERTOKEN} -t glrunner .
	docker stop glrunner && docker rm glrunner
	docker run -d --name glrunner --restart always grunner
	docker exec -it glrunner gitlab-runner register

gitlab_runner_unregister:
	@echo "[+] Unregistering gitlab runners"
	docker exec -it glrunner gitlab-runner unregister --all-runners
